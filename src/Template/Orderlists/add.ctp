<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($orderlist,[
        "id"=>"checkvali"
    ]) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
        echo $this->Form->control('iduser',["required"=>false]);
        echo $this->Form->control('name',["required"=>false]);
        echo $this->Form->control('totalprice',["required"=>false]);
        echo $this->Form->control('createat',["id"=>"datepicker","autocomplete"=>"off",""]);
        echo $this->Form->control('address',["required"=>false]);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
