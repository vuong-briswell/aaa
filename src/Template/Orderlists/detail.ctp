<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New OrderList'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('STT') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Tên sản phẩm') ?></th>
            <th scope="col"><?= $this->Paginator->sort('số lượng') ?></th>
            <th scope="col"><?= $this->Paginator->sort('giá') ?></th>
            <th scope="col"><?= $this->Paginator->sort('tổng') ?></th>

        </tr>
        </thead>
        <tbody>

        <?php $sount=1;$total=0;$all=0; foreach
        ($query as $user):
                $total=$user->price*$user->qty;
        $all  +=$total;
            ?>
            <tr>
                <td><?= $sount++ ?></td>
                <td><?= h($user->name)?></td>
                <td><?= h($user->qty) ?></td>
                <td><?= h($user->price) ?></td>
                <td><?= h($total) ?></td>

            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="4 " style="text-align: right"><h5>Tổng :</h5></td>
            <td colspan="1" style="text-align: right"><?=$all?></td>
        </tr>

        </tbody>
    </table>

</div>
