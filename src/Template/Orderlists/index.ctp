<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New OrderList'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Orderlists') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('STT') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Tên người mua') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Tổng giá') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Ngày mua') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Địa chỉ') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Action') ?></th>
        </tr>
        </thead>
        <tbody>

        <?php $sount=1; foreach ($orderlists as $user): ?>
            <tr>
                <td><?= $sount++ ?></td>
                <td><?= h($user->name) ?></td>
                <td><?= h($user->totalprice) ?></td>
                <td><?= h($user->createat) ?></td>
                <td><?= h($user->address) ?></td>
                <td><?= $this->html->link(__("Order detail"),["action"=>"detail",$user->id])?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>
