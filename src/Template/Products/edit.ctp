<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $product->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($product,['type'=>"file"]) ?>
    <fieldset>
        <legend><?= __('Edit Product') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('price');
            ?>

        <label>
            Thêm ảnh:
            <input type="file" style="display: none" name="img" id="file1">
            <img src="/UserBT/img/<?= $product->img?>" id="preimg1">
        </label>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<script>
    var str = "Apples are round, and apples are jASDuicy.";
    var splitted = str.split(" ").pop().toLowerCase();

    console.log(splitted);
    $('#file1').change(function(){
        var test_value=$('#file1').val();
        //   console.log(test_value.split('.'));
        var extension=test_value.split('.').pop().toLowerCase();
        // console.log(test_value);
        if($.inArray(extension,['png','gif','jpeg','jpg'])== -1)
        {
            alert("File ảnh không hợp lệ");
            return false;
        }
        else
        {
            var preview = document.querySelector('#preimg1');
            var file    = document.querySelector('#file1').files[0];
            var reader  = new FileReader();
            reader.addEventListener("load", function () {
                preview.src = reader.result;
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
            return false;
        }
    });



</script>
