<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>

<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create(null,[
        'id'=>"checkForm"
    ]) ?>
    <fieldset>
        <legend><?= __('Add Product') ?></legend>
        <?php
        echo $this->Form->control('name',["class"=>"optdate"]);
        echo $this->Form->control('price',["class"=>"zip"]);
        ?>
    </fieldset>

    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<script>
// $("#checkForm").validate({
//     rules: {
//         "name": {
//             required: true,
//         },
//         "price": {
//             required: true
//         }
//     },
//     messages:{
//         "name":{
//             required:"asd"
//         }
//     },
//     submitHandler:function(form) {
//         //code in here
//         alert("ok");
//     }
//
// });

// connect it to a css class
// add a method. calls one built-in method, too.
// jQuery.validator.addMethod("optdate", function(value, element) {
//         return jQuery.validator.methods['date'].call(
//             this,value,element
//         )||value==("0000/00/00");
//     }, "Please enter a valid date."
// );
//
// // connect it to a css class
// jQuery.validator.addClassRules({
//     optdate : { optdate : true ;required:true}
// });
// $('#checkForm').validate();
</script>
