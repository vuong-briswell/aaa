<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user,[
        "id"=>"checkvali"
    ]) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('first_name',["required"=>false]);
            echo $this->Form->control('last_name',["required"=>false]);
            echo $this->Form->control('birthday',["id"=>"datepicker","autocomplete"=>"off",""]);
            echo $this->Form->control('email',["required"=>false]);
            echo $this->Form->control('password',["required"=>false]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>
    $(document).ready(function(){
        $("#datepicker").datepicker();
        jQuery.validator.addMethod("domain", function(value, element) {
            return this.optional(element) || /([a-z][^0-9])/.test(value);
        }, "chỉ được nhập [a-z]");
        $("#asd").validate({
            rules: {
                "first_name": {
                    required:true,
                    domain:true,
                    maxlength:50

                },
                "last_name":{
                    required:true,
                    maxlength:25,
                    domain:true,
                },
                "birthday":{
                    required:true,
                    date:true
                },
                "email":{
                    required: true,
                    email:true,

                },
                "password":{
                    required: true,
                }
            },
            messages:{

                first_name:{
                    required:"Required",
                    maxlength:"max 50 characters"
                },
                last_name:{
                    required:"Required",
                    maxlength:"max 25 character"
                },
                email:{
                    required:"Required",
                    email:"Require email"
                },
                password:{
                    required:"Required",
                }
            }
        });
    });

</script>
