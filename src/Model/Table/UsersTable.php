<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->notEmptyString('id', null, 'create');

        $validator->requirePresence("first_name","create")
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->notEmptyString('first_name',"không được để chống","create")
        ->add("first_name",[
            "length"=>[
                "rule"=>["maxLength",50],
                "message"=>"không vượt quá 50 chatacters"
            ]
        ])
            ->add("first_name","custom",[
                "rule"=>function($value,$context)
                {
                    if(preg_match("/^[a-z]+$/",$value,$matches))
                    {
                        return true;
                    }
                    else{
                        return "phải là chữ không số";
                    }
                    return true;
                }
            ])
        ;

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->notEmptyString('last_name',"Không được để chống","create")
        ->add("last_name",[
            "length"=>[
                "rule"=>["maxLength",25],
                "message"=>"không vượt quá 25 chatacters"
            ]
        ])
        ->add("last_name","custom",[
            "rule"=>function($value,$context)
            {
                if(preg_match("/^[a-z]+$/",$value,$matches))
                {
                    return true;
                }
                else {
                    return "phải là chữ không số";
                }
                return true;
            },
        ]);

        $validator
            ->scalar('birthday')
            ->maxLength('birthday', 255)
            ->notEmptyString('birthday',"Không được để chống","create");

        $validator
            ->email('email')
            ->notEmptyString('email',"Không được để chống","create")
        ->add("email","valiFormat",[
            "rule"=>"email",
            "message"=>"Email đã tồn tại"
        ])
        ->add("email",[
            "length"=>[
                "rule"=>["maxLength",75],
                "message"=>"không vượt quá 75 kí tự"
            ]
        ]);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->notEmptyString('password',"không được để chống","create");

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
